import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  data: any;
  listData = [
    {
      id: 1,
      name: 'demo 1',
      show: true
    },
    {
      id: 2,
      name: 'demo 2',
      show: true
    },
    {
      id: 3,
      name: 'demo 3',
      show: false
    },

  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data = navParams.get("data");
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

}
