import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthenticationsPage } from './authentications';

@NgModule({
  declarations: [
    AuthenticationsPage,
  ],
  imports: [
    IonicPageModule.forChild(AuthenticationsPage),
  ],
})
export class AuthenticationsPageModule {}
