import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, ToastController } from 'ionic-angular';


/**
 * Generated class for the AuthenticationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-authentications',
  templateUrl: 'authentications.html',
})
export class AuthenticationsPage {

  username: string = '';
  tabs: any = 'sign-in';
  formData: any = {
    username: '',
    password: '',
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController, 
    public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthenticationsPage');
  }

  processLogin() {
    let username = 'demo';
    let password = 'demo';

    if (this.formData.username == username && this.formData.password == password) {
      let params = { data: 'Hello Demo' };
      this.navCtrl.push("DashboardPage", params);
    }
    else {
      this.showAlert();
    }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Information',
      subTitle: 'Username or Password Invalid',
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Username or Password Invalid',
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }
}
