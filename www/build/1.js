webpackJsonp([1],{

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationsPageModule", function() { return AuthenticationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentications__ = __webpack_require__(273);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AuthenticationsPageModule = /** @class */ (function () {
    function AuthenticationsPageModule() {
    }
    AuthenticationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__authentications__["a" /* AuthenticationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__authentications__["a" /* AuthenticationsPage */]),
            ],
        })
    ], AuthenticationsPageModule);
    return AuthenticationsPageModule;
}());

//# sourceMappingURL=authentications.module.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AuthenticationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AuthenticationsPage = /** @class */ (function () {
    function AuthenticationsPage(navCtrl, navParams, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.username = '';
        this.tabs = 'sign-in';
        this.formData = {
            username: '',
            password: '',
        };
    }
    AuthenticationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AuthenticationsPage');
    };
    AuthenticationsPage.prototype.processLogin = function () {
        var username = 'demo';
        var password = 'demo';
        if (this.formData.username == username && this.formData.password == password) {
            var params = { data: 'Hello Demo' };
            this.navCtrl.push("DashboardPage", params);
        }
        else {
            this.showAlert();
        }
    };
    AuthenticationsPage.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Information',
            subTitle: 'Username or Password Invalid',
            buttons: ['OK']
        });
        alert.present();
    };
    AuthenticationsPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Username or Password Invalid',
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    AuthenticationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-authentications',template:/*ion-inline-start:"/Users/poncoherbowo/ionic/my-training/src/pages/authentications/authentications.html"*/'<!--\n  Generated template for the AuthenticationsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Authentications</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div padding>\n    <ion-segment [(ngModel)]="tabs">\n      <ion-segment-button value="sign-in">\n        SIGN IN\n      </ion-segment-button>\n      <ion-segment-button value="sign-up">\n        SIGN UP\n      </ion-segment-button>\n    </ion-segment>\n  </div>\n\n  <div [ngSwitch]="tabs">\n    <ion-list *ngSwitchCase="\'sign-in\'">\n      <ion-item>\n        <ion-label floating>Username</ion-label>\n        <ion-input\n          type="text"\n          value=""\n          [(ngModel)]="formData.username"\n        ></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Password</ion-label>\n        <ion-input\n          type="password"\n          value=""\n          [(ngModel)]="formData.password"\n        ></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <button ion-button (click)="processLogin()">Sign In</button>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/poncoherbowo/ionic/my-training/src/pages/authentications/authentications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], AuthenticationsPage);
    return AuthenticationsPage;
}());

//# sourceMappingURL=authentications.js.map

/***/ })

});
//# sourceMappingURL=1.js.map